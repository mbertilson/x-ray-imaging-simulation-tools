How to get started

This version of the code was tested with python 3.11.5 (main, Sep 11 2023, 08:19:27).
The python libraries below are needed to run the simulation example. The numbers indicate the version used in testing.

- numpy (1.26.2)
- matplotlib (3.8.0)
- scipy (1.11.4)
- scikit-image (0.20.0)
- Pillow (10.0.1)
- time (part of python 3.11)
- numba (0.58.1)
- xraydb (4.5.0)
- dill (0.3.6)
- pathlib (1.0.1)


The materials.dat file provided in the repository has additional materials (e.g. tissue types) defined which are not included in the file provided in the xraydb installation. 
Place this file in the xraydb library folder. To find the folder use these commands: 

import xraydb as xdb
xdb.__file__

On my system the path is: '/Users/Michael/anaconda3/lib/python3.11/site-packages/xraydb/'. 
For the library to recognize the new materials.dat file delete the file materials.cpython-311.pyc located in the xraydb/pycache folder. 

A new one will be compiled when the xraydb library imported. You will have to repeat this step every time you make edits to the materials.dat file.

The phantom image (Brain section 0.5 mm.tif file) and the DPC_simulation.py file from the repository should be placed in the same folder as the simulation example.py file.
 
Set Test = True in the Simulation example.py file and run the code. This test mode is useful for verifying that the other parameters such as sampling and imaging setup parameters are set up correctly and produce expected results. Figure 3 compares the reconstructed phase with the actual phase. They should match and there should be some margin around the object where there is no phase shift. If that is not the case adjust the sampling size (dx), the number of samples (Nx), the pixel size of the phantom and the binning (Bin).

Once the test mode produces good results you can run a full CT simulation by setting Test = False.

Calculating the projections can take some time so if you want to simulate the same geometry but with different dose conditions you can run choose to save the projection simulation results for that geometry in one first run. Just make sure to reset the kernel before running the simulation (save mode saves all the variables of the session). Now you can choose to load the saved projection results and change any property in the Define_reconstruction object, e.g the photons_per_pixel parameter to simulate that scenario. Note you cannot use this if you change any of the objects of the imaging setup or it's geometry. You would have to run the enitre simulation from scratch. 















