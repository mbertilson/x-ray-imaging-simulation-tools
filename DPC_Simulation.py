'''
DPC simulation library
Author: Michael Bertilson, 2022
'''

import numpy as np
import xraydb as xdb
from PIL import Image
from scipy import interpolate
import matplotlib.pylab as plt
import matplotlib.cm as cm
from skimage.filters import gaussian
from numba import jit, prange
import sys
from copy import copy
import dill
from pathlib import Path
from pprint import pprint
from scipy.optimize import curve_fit
# from io import StringIO

# Physical constants
q_e = 1.60217e-19 # [J], Electron charge
r_e = 2.82e-15 # [m], Classical electron radius
h = 6.6261e-34 # [J*s]
c = 2.998e8 # [m/s]



def Get_transmittance_and_phase(material, thickness_m, energy_keV):
    wave_m = h*c/(energy_keV*1e3*q_e) # Wavelength [m]
    formula, density = xdb.get_material(material)
    delta, beta, att_cm = xdb.xray_delta_beta(formula, density, energy_keV*1e3) 
    
    # att_cm from the function above does not reproduce values from NIST
    # material_mu with kind='total' does match NIST
    att_cm = 1/xdb.material_mu(material, energy_keV*1e3, kind='total')
    beta_total = wave_m/(4*np.pi*att_cm*1e-2)
    
    # n = 1 - delta - i*beta, E = exp(i*2*pi*n*L/lambda), I = abs(E)^2 = E*conj(E)
    E = np.exp(-1j*2*np.pi*thickness_m*(0 - delta - 1j*beta_total)/wave_m) # 0 - delta because phase is calculated relative vacuum
    Transmittance = np.abs(E)**2
    Phase = np.angle(E)
    return Transmittance, Phase

def Add_noise(s):
    s = np.random.poisson(s)
    return s


@jit(nopython=True, fastmath=True, parallel=True)
def Diffusion_filter(I, sigma):

    Nk = int(3*np.ceil(np.max(sigma)))
    if (Nk < 2):
        Nk = int(2)
    
    # Pad input image so convolution can be performaed all the way to edge of original image (np.pad not supported by numba)
    I = np.concatenate((np.ones(Nk)*I[0], I, np.ones(Nk)*I[-1]))
    sigma = np.concatenate((np.zeros(Nk), sigma, np.zeros(Nk)))
    
    Nx = I.size
    x = np.arange(2*Nk + 1) - Nk
    kernel = np.zeros(2*Nk + 1)
    Idiff = np.zeros(Nx)
    
    for i in prange(Nk, Nx - Nk):
        if sigma[i]==0: 
            kernel = np.exp(-0.5*((x)/0.25)**2)
        else:  
            kernel = np.exp(-0.5*((x)/sigma[i])**2)
        kernel = kernel/np.sum(kernel)
        Idiff[i] = np.sum(I[(i-Nk):(i+Nk+1)]*kernel)
    
    Idiff = Idiff[Nk:(Nx-Nk)] # Trim off edge padding
    
    return Idiff


def Print_system_info(sim, setup, source, G0, G1, phantom, G2, detector):
    np.set_printoptions(threshold=3, edgeitems=1)
    print('\nSimulation parameters: ')
    pprint(vars(sim))
    print('\nExperimental setup: ')
    pprint(vars(setup))
    print('\nSource: ')
    pprint(vars(source))
    print('\nG0 grating:')
    pprint(vars(G0))
    print('\nG1 grating:')
    pprint(vars(G1))
    print('\nPhantom:')
    pprint(vars(phantom))
    print('\nG2 grating:')
    pprint(vars(G2))
    print('\nDetector:')
    pprint(vars(detector))
    

def Save_simulation_info(File_path, sim, setup, source, G0, G1, phantom, G2, detector):
    np.set_printoptions(threshold=3, edgeitems=1)
    with open(File_path,'w') as file:
        print('\nSimulation parameters:\n', file=file)
        pprint(vars(sim), file)
        print('\nExperimental setup:\n', file=file)
        pprint(vars(setup), file)
        print('\nSource:\n', file=file)
        pprint(vars(source), file)
        print('\nG0:\n', file=file)
        pprint(vars(G0), file)
        print('\nG1:\n', file=file)
        pprint(vars(G1), file)
        print('\nPhantom:\n', file=file)
        pprint(vars(phantom), file)
        print('\nG2:\n', file=file)
        pprint(vars(G2), file)
        print('\nDetector:\n', file=file)
        pprint(vars(detector), file)


def Get_visibility(Fringes):
    return (Fringes.max() - Fringes.min())/(Fringes.max() + Fringes.min())


def Render_detector_image(x, I, PSF=None):
    
    I = I[x.padding: -x.padding]
    
    
    if PSF is not None:
        PSF = PSF[x.padding: -x.padding]
        I_det = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fft(PSF)*np.fft.fft(I))))
        I_det = I_det*np.sum(I)/np.sum(I_det)
    else:
        I_det = I
    
    # I_det = I_det[x.padding: -x.padding]
    # I_pix = I_det.reshape(-1, x.samples_per_pixel).mean(axis=1)
    I_pix = I_det.reshape(-1, x.samples_per_pixel)[:,int(x.samples_per_pixel/2)] # Takes the middle sample, no blurring by taking mean
    # I_pix = I_det.reshape(-1, x.samples_per_pixel)[:,3:4].mean(axis=1)


    return I_pix


def Get_air_energy_absorption(energy_keV):
    '''
    input parameter: energy_keV : X-ray energy in keV
    Returns the energy absorption coefficient for dry air [cm2/g]. 
    Interpolates tabulated values from NIST.
    '''
    E = np.array([10, 15, 20, 30, 40, 50, 60, 80, 100, 150, 200, 300])
    mu = np.array([4.74, 1.33, 5.39e-1, 1.54e-1, 6.83e-2, 4.1e-2, 3.04e-2, 2.41e-2, 2.33e-2, 2.5e-2, 2.67e-2, 2.87e-2])
    return np.exp(np.interp(np.log(energy_keV), np.log(E), np.log(mu)))
    


def Normalize_scan_data(scan_data):
    norm_scan_data = np.zeros(scan_data.shape)
    for j in range(scan_data.shape[1]):
        norm_scan_data[:,j] = scan_data[:,j]/np.max(scan_data[:,j])
    return norm_scan_data


class Define_simulation:
    '''
    Test mode runs simulation with 1 projection and displays test plots
    Set ignore_diffustion to False to use local diffusion coefficient for modeling small angle scattering (slower)
    If save_reslts is set true the workspace is saved for later use in reconstruction. Simulated projections can also be saved as tifs.
    '''
    def __init__(self, load_data, data_folder, test_mode=True, ignore_diffusion=True, samples_per_pixel=2, id_number='', save_results=True, include_tifs=True):
        self.load_data = load_data
        self.data_folder = data_folder
        self.id_number = id_number
        self.samples_per_pixel = int(samples_per_pixel)
        self.ignore_diffusion = ignore_diffusion
        
        if self.load_data: 
            dill.load_session(self.data_folder / Path(self.id_number + ' - Simulation data.pkl'))
            self.load_data = True # Must set this back to True incate the loaded will set to False
            self.data_folder = data_folder 
            print('Simulation results loaded')
            
        self.test_mode = test_mode
        self.save_results = save_results
        self.include_tifs = include_tifs
        
        

class Define_reconstruction:
    '''
    G2 Simulates image formation done with a G2 grating instead of a high resolution detector.
    Available fitting methods are: 'DFT' or 'Scipy'. DFT is faster than scipy curve_fit.
    maks_type can be 'Square' or 'Cosine'
    comb_ratio (slit/period) affects the visibility and noise in calculated phase stepping curves.

    '''
    def __init__(self, data_folder, add_noise, total_dose_Gy=None, slice_thickness_m=5e-3, photons_per_pixel=None, mask_type='Comb', comb_ratio=0.25, fitting_method='DFT', G2_based=False, id_number='', save_results=True):
        self.data_folder = data_folder
        self.add_noise = add_noise
        self.total_dose_Gy = total_dose_Gy
        self.projection_dose_Gy = None
        self.slice_thickness_m = slice_thickness_m
        self.slice_mass_kg = None
        self.photons_per_pixel = photons_per_pixel
        self.photons_per_bin = None
        self.fitting_method = fitting_method
        self.mask_type = mask_type
        self.comb_ratio = comb_ratio
        self.id_number = id_number
        self.save_results = save_results
        self.G2_based = G2_based
        self.KAP = None # Kerma air product
        self.bin_area_cm2 = None 


class Define_setup:
    '''
    Formulas for calculating the G1 position and period assumes it is a phase grating. Source is at origin.
    '''
    def __init__(self, energy_keV, talbot_order, G2_position_m, G2_period_um, object_position_m):
        self.energy_keV = energy_keV
        self.talbot_order = talbot_order
        self.G2_position_m = G2_position_m
        self.G2_period_um = G2_period_um
        self.detector_position_m = G2_position_m
        photon_energy_J = energy_keV*1e3*q_e
        wave_m = h*c/photon_energy_J # Wavelength [m]
        self.G1_period_um = (2/(1/(G2_period_um*1e-6) + talbot_order*G2_period_um*1e-6/(2*wave_m*G2_position_m)))/1e-6
        self.G1_position_m = G2_position_m*self.G1_period_um/(2*G2_period_um)
        if object_position_m >= self.G1_position_m:
            self.object_position_m = object_position_m
        else:
            print('The object cannot be positioned before the G1 grating in an inverse setup. \nG1 position: %0.3f' % (self.G1_position_m))
            sys.exit()
        self.source_magnification = (G2_position_m - object_position_m)/object_position_m
        self.G0_magnification = (G2_position_m - self.G1_position_m)/self.G1_position_m
        self.object_magnification = G2_position_m/object_position_m
        self.G0_position_m = 0
        self.G0_period_um = G2_period_um/self.G0_magnification
        self.max_acceptable_phase_gradient = np.pi*self.G2_period_um*1e-6/(wave_m*(self.detector_position_m - self.object_position_m))
        self.visibility = None


class Define_source:
    def __init__(self, energy_keV, size_um):
        self.energy_keV = energy_keV
        self.size_um = size_um # FWHM of Gaussian intensity distribution
        self.photon_energy_J = energy_keV*1e3*q_e
        self.wave_m = h*c/self.photon_energy_J # Wavelength [m]


class Define_grating:
    '''
    Assumnes one material and vacuum/air in the space between. 
    Shape can be Square or Cosine. 
    '''
    def __init__(
        self, position_m, material, period_um=None, thickness_um=None, shape='Square',
        slit_period_ratio=0.5, smooth_um=0, phase=np.pi, transmittance=0, energy_keV=None):

        self.position_m = position_m
        self.material = material
        self.period_um = period_um
        self.thickness_um = thickness_um
        self.shape = shape
        self.slit_period_ratio = slit_period_ratio
        self.smooth_um = smooth_um
        self.energy_keV = energy_keV
        
        if material == 'Custom':
            self.phase = phase
            self.transmittance = transmittance
        else:
            self.wave_m = h*c/(energy_keV*1e3*q_e) # Wavelength [m]
            self.formula, self.density = xdb.get_material(material)
            delta, beta, att_cm = xdb.xray_delta_beta(self.formula, self.density, energy_keV*1e3)
            
            self.delta = delta
            # att_cm from the function above does not reproduce values from NIST
            # material_mu with kind='total' does match NIST
            att_cm = 1/xdb.material_mu(material, energy_keV*1e3, kind='total')
            self.beta_total = self.wave_m/(4*np.pi*att_cm*1e-2)
            
            # n = 1 - delta - i*beta, E = exp(i*2*pi*n*L/lambda), I = abs(E)^2 = E*conj(E)
            self.transmittance, self.phase = Get_transmittance_and_phase(material, thickness_um*1e-6, energy_keV)

        self.signal_ratio = slit_period_ratio/(self.transmittance*(1 - slit_period_ratio) + slit_period_ratio) # Imaging signal vs stray light/BG
        self.modulation = (1 - self.transmittance)/(1 + self.transmittance)

    def Transmission(self, x_m, magnification=1, shift_m=0):
        dx_m = np.mean(np.diff(x_m))
        x_idx = np.round((x_m - x_m.min())/dx_m).astype('int')
        shift_idx = int(round(shift_m/dx_m))
        period_idx = int(np.round(self.period_um*1e-6*magnification/dx_m))
        
        
        if self.shape == 'Cosine':
            T = np.sqrt((np.cos(2*np.pi*(x_m + shift_m)/(self.period_um*1e-6))+1)*(1-self.transmittance)/2 + self.transmittance)
        else:
            if self.material == 'Custom':
                T = T = np.mod(x_idx + shift_idx, period_idx) 
                T = np.where(T < self.slit_period_ratio*T.max(), 1, np.sqrt(self.transmittance)*np.exp(-1j*self.phase))
            else:
                T = np.mod(x_idx + shift_idx, period_idx)
                # T = np.where(T <= self.slit_period_ratio*T.max(), 1, np.sqrt(self.transmittance)*np.exp(-1j*self.phase))
                T = np.where(T < self.slit_period_ratio*T.max(), 0, 1)
                
                if self.smooth_um>0:
                    kernel = np.where((x_m>=-self.smooth_um*1e-6*magnification/2)*(x_m<self.smooth_um*1e-6*magnification/2), 1, 0)
                    T = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fft(T)*np.fft.fft(kernel))))
                
                T = T - np.min(T)
                T = T/np.max(T)
                
                # n = 1 - delta - i*beta, E = exp(i*2*pi*n*L/lambda), I = abs(E)^2 = E*conj(E)
                T = np.exp(-1j*2*np.pi*T*self.thickness_um*1e-6*(0 - self.delta - 1j*self.beta_total)/self.wave_m)
                self.phase = np.max(np.abs(np.angle(T)))
                self.transmittance = np.min(np.abs(T)**2)

        return T


class Define_detector:
    '''
    Bins are the final binned pixels (macro-pixels). Set the approximate bin_size_um and the nearest possible
    bin_size will be calculated. 
    The PSF_width_um is the FWHM of the location uncertanty. 
    ''' 
    def __init__(self, pixel_size_um, PSF_width_um, approx_bin_size_um, setup, phantom):
        self.pixel_size_um = pixel_size_um
        self.approx_bin_size_um = approx_bin_size_um
        self.PSF_width_um = PSF_width_um # FWHM of Lorentzian
        self.pixels_per_G2_period = int(setup.G2_period_um/pixel_size_um)
        self.periods_per_bin = int(np.round(approx_bin_size_um/(pixel_size_um*self.pixels_per_G2_period)))
        self.pixels_per_bin = int(np.round(self.pixels_per_G2_period*self.periods_per_bin))
        self.bin_size_um = self.pixels_per_bin*pixel_size_um
        self.bins = int(np.ceil(phantom.size_m*1e6*setup.object_magnification/self.bin_size_um/2))*2 # Want this to be an even number
        self.pixels = int(self.bins*self.pixels_per_bin)
        self.position_m = setup.G2_position_m
        self.size_m = self.pixels*self.pixel_size_um*1e-6
        self.obj_bin_size_um = self.bin_size_um/setup.object_magnification


class Define_angles:
    '''
    If the size_resolution_ratio is set it overrides the manual custom settings
    '''
    def __init__(self, N=None, range_deg=None, size_resolution_ratio=None):
        if size_resolution_ratio is not None:
            self.N = int(np.pi*size_resolution_ratio)
            self.range_deg = 180
        else:
            self.N = int(N)
            self.range_deg = range_deg

        if N == 1:
            self.deg = np.array([0])
            self.rad = self.deg*np.pi/180
        else:
            self.deg = np.linspace(0, self.range_deg, self.N, endpoint=False)
            self.rad = np.linspace(0, self.range_deg*np.pi/180, self.N, endpoint=False)


class Define_coordinates:
    '''
    Calculate padding needed due to diffraction effects at edge of field during propagation.
    All coordinates in units of meters.
    '''
    def __init__(self, samples_per_pixel, setup, source, detector):
        self.samples_per_pixel = samples_per_pixel
        self.det_dx = detector.pixel_size_um*1e-6/samples_per_pixel

        # Find vector size with only small prime factors (=<3) for faster FFTs. 
        # minimum_padding = int(source.wave_m*(setup.G2_position_m - setup.G1_position_m)/(2*self.det_dx**2)) # Don't remember why
        minimum_padding = int(1e-3/self.det_dx) # from inspection
        extra_padding = int(self.samples_per_pixel*detector.pixels/2) # Extra padding 
        minimum_samples = self.samples_per_pixel*detector.pixels + 2*(minimum_padding + extra_padding)
        np2 = len(np.binary_repr(minimum_samples)) # Next power of 2 > minimum samples        
        K,L = np.meshgrid(np.arange(np2+1), np.arange(np2+1))
        N = 2**K*3**L
        N[N<minimum_samples] = N.max()
        self.samples = N.min()
        
        self.minimum_padding = minimum_padding
        self.padding = int((self.samples - detector.pixels*self.samples_per_pixel)/2)
        self.det = np.linspace(-0.5*(self.samples-1)*self.det_dx, 0.5*(self.samples-1)*self.det_dx, self.samples)
        self.G1 = self.det*(setup.G1_position_m/setup.G2_position_m)
        self.obj = self.det/(setup.object_magnification)
        self.pix = np.reshape(self.det[self.padding:-self.padding], [-1, samples_per_pixel]).mean(axis=1)
        # self.pix = np.reshape(self.det[self.padding:-self.padding], [-1, samples_per_pixel])[:,4]
        # self.pix = np.reshape(self.det[self.padding:-self.padding], [-1, samples_per_pixel])[:,3:4].mean(axis=1)
        self.bin = np.reshape(self.pix, [-1, detector.pixels_per_bin]).mean(axis=1)

        print('Sampling element @ object plane: %.3f um' % (self.det_dx/1e-6/setup.object_magnification))
        print('Sampling element @ detector plane: %.3f um' % (self.det_dx/1e-6))

        
class Define_PSF:
    '''
    Calculates the PSFs scaled to the detector plane
    shape parameter can be:
        'Custom1': 1 um FWHM PSF from Christel Sundberg 
        'Custom2': 2.5 um PSF from Christel Sundberg
        'Lorentzian': Lorentzian PSF with FWHM defined by detector.PSF_width_um parameter
        'Gaussian': Gaussian PSF with FWHM defined by detector.PSF_width_um parameter
    '''
    def __init__(self, x, source, G0, detector, setup, shape='None', include_pixel=True, smooth=0):
        self.x = x.det
        self.dx = x.det_dx
        self.shape = shape
        self.include_pixel = include_pixel
        G0_T = abs(G0.Transmission(self.x, setup.G0_magnification))**2
        self.source = np.exp(-0.5*((self.x - self.dx/2)/(source.size_um*1e-6*setup.source_magnification/2.35))**2)*G0_T
        self.detector = np.zeros(self.x.size)
        self.detector[np.argmin(np.abs(self.x))] = 1
        self.pixel = np.where((self.x < detector.pixel_size_um*1e-6/2) & (self.x > -detector.pixel_size_um*1e-6/2), 1, 0)
        
        if shape == 'Custom1':
            Data = np.array([
                [-6.988, 0.000],[-5.958, 0.000],[-5.000, 0.001],[-4.114, 0.006],[-3.263, 0.011],[-2.425, 0.024],
                [-1.683, 0.057],[-1.024, 0.117],[-0.713, 0.232],[-0.533, 0.361],[-0.425, 0.492],[-0.341, 0.612],
                [-0.257, 0.737],[-0.186, 0.842],[-0.090, 0.949],[0.000, 1.000],[0.006, 0.996],[0.138, 0.927],
                [0.186, 0.846],[0.257, 0.743],[0.353, 0.616],[0.425, 0.494],[0.545, 0.360],[0.701, 0.235],
                [1.024, 0.124],[1.599, 0.055],[2.329, 0.027],[3.036, 0.016],[3.731, 0.009],[4.377, 0.004],
                [5.000, 0.001],[5.754, 0.001],[6.401, 0.000],[7.000, 0.000]]).transpose()
            xs = Data[0]*1e-6
            cs = Data[1]
            PSF = interpolate.interp1d(xs, cs, kind='linear', fill_value="extrapolate")
            self.detector = PSF(self.x)
            
        if shape == 'Custom2':
            Data = np.array([[-9.000, 0.000],[-8.000, 0.000],[-6.968, 0.003],[-5.964, 0.009],[-4.987, 0.025],[-4.089, 0.060],
                [-3.256, 0.129],[-2.503, 0.229],[-1.856, 0.330],[-1.367, 0.447],[-0.997, 0.600],[-0.627, 0.780],
                [-0.390, 0.900],[0.000, 1.000],[0.007, 0.989],[0.443, 0.892],[0.641, 0.785],[0.918, 0.611],
                [1.394, 0.454],[1.830, 0.339],[2.424, 0.245],[3.124, 0.148],[3.877, 0.075],[4.987, 0.027],
                [6.004, 0.009],[7.008, 0.002],[8.000, 0.000],[9.000, 0.000]]).transpose()
            xs = Data[0]*1e-6
            cs = Data[1]
            PSF = interpolate.interp1d(xs, cs, kind='linear', fill_value="extrapolate")
            self.detector = PSF(self.x)
            
        if shape == 'Lorentzian':
            self.detector = (1 + ((self.x - self.dx/2)/(detector.PSF_width_um*1e-6/2))**2)**-1
                        
        if shape == 'Gaussian':
            self.detector = np.exp(-0.5*(2.36*(self.x-self.dx/2)/(detector.PSF_width_um*1e-6))**2)
        
        
        if include_pixel:
            self.system = np.abs((np.fft.ifft(np.fft.fft(self.source)*np.fft.fft(self.detector)*np.fft.fft(self.pixel))))
        else:
            self.system = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fft(self.source)*np.fft.fft(self.detector))))
        
        if smooth>0:
            kernel = np.where((self.x>=-smooth*self.dx)*(self.x<smooth*self.dx), 1, 0)
            self.system = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fft(self.system)*np.fft.fft(kernel))))
        
        self.system = self.system/np.max(self.system)


    def Show(self):
        plt.figure()
        # plt.plot(x.det/1e-6, I_G1, 'k', label='Talbot pattern')
        plt.plot(self.x/1e-6, self.source, 'b', label='Source')
        plt.plot(self.x/1e-6, self.detector, 'r', label='Detector')
        if self.include_pixel: plt.plot(self.x/1e-6, self.pixel, 'y', label='Pixel')
        plt.plot(self.x/1e-6, self.system, 'g', label='System')
        plt.xlim([-1000, 1000])
        plt.title('PSFs')
        plt.xlabel('x [um]')
        plt.legend(loc='upper right')
        plt.show()


def Propagation_kernel(x_m, wave_m, radius_m, distance_m):
    '''
    Calculates the propagation kernel to propagate the a wavefront defined by its wavelength (wave_m)
    and radius of curvature (radius_m) a given distance (distance_m).
    '''
    dx_m = x_m[1]-x_m[0] 
    du = 1/(x_m.size*dx_m)
    u = (x_m/dx_m)*du
    distance_m = distance_m*radius_m/(distance_m + radius_m) # distance adjusted for sphereical wavefront
    kernel = np.exp(-1j*2*np.pi**2*distance_m*u**2/(2*np.pi/wave_m))
    
    a = x_m.max()-x_m.min()
    F = (4.78e-6)**2/(distance_m*wave_m)

    print('Distance: %.3f, Fresnel number: %.3E, Propagation criterion (> distance): %0.3f' % (distance_m, F, x_m.size*dx_m**2/wave_m))
    print('Max phasor change per element: %0.3f' % (np.pi*distance_m*wave_m*(u[-1]**2 - u[-2]**2)))
    
    return kernel


def Propagation_test(x, setup, source, G1):
    d_talbot = setup.talbot_order*(setup.G1_period_um*1e-6/2)**2/(2*source.wave_m)
    d_talbot = setup.G1_position_m*d_talbot/(setup.G1_position_m - d_talbot)
    
    x_m = 10*(np.arange(1000)-500)*x.det_dx*setup.G1_position_m/setup.G2_position_m
    F = Define_EM_field(x_m, wave_m=source.wave_m, radius_m=G1.position_m, E=G1.Transmission(x_m, 1))
    d = np.linspace(0, d_talbot, 500)
    
    I = np.zeros((1000,500))
    
    for i in range(500):
        Fp = F.Propagate(distance_m=d[i], same_size=True)
        I[:,i] = Fp.I
    
    plt.figure()
    plt.pcolor((d + G1.position_m), x_m/1e-6/G1.period_um, I, cmap=cm.gray)
    plt.xlabel('Propagation distance [m]')
    plt.ylabel('x [1/G1 period]')
    plt.xlim(0, d.max() + G1.position_m)


class Define_EM_field:
    def __init__(self, x_m, wave_m, radius_m, E):
        self.wave_m = wave_m
        self.radius_m = radius_m
        self.E = E
        self.I = np.abs(self.E)**2
        self.x_m = x_m

    def Propagate(self, distance_m, kernel=None, same_size=False):
        # If you have already precalculated the kernal for the propagation use that, it is faster.
        # If same_size is set to True the propagated field will be interpolated at the locations
        # defined in initial field (x_m).
        if kernel is None:
            kernel = Propagation_kernel(self.x_m, self.wave_m, self.radius_m, distance_m)
        
        Propagated_field = copy(self)
        Propagated_field.E = np.fft.ifft(np.fft.fft(self.E)*np.fft.fftshift(kernel))
        Propagated_field.I = np.abs(Propagated_field.E)**2
        I_ratio = np.sum(self.I)/np.sum(Propagated_field.I)
        Propagated_field.I = I_ratio*Propagated_field.I # Normalize intensity
        Propagated_field.E = np.sqrt(I_ratio)*Propagated_field.E # Normalize EM field
        Propagated_field.radius_m = self.radius_m + distance_m
        Propagated_field.x_m = self.x_m*(self.radius_m + distance_m)/self.radius_m

        if same_size:
            Propagated_field.E = np.interp(self.x_m, Propagated_field.x_m, Propagated_field.E)
            Propagated_field.I = np.interp(self.x_m, Propagated_field.x_m, Propagated_field.I)
            Propagated_field.x_m = self.x_m

        return Propagated_field

    def Show(self):
        plt.figure()
        plt.plot(self.x_m/1e-3, self.I)
        plt.xlabel('location [mm]')
        plt.title('Intensity')

        plt.figure()
        plt.plot(self.x_m/1e-3, np.angle(self.E))
        plt.xlabel('location [mm]')
        plt.title('Phase')


class Define_phantom:
    '''
    The phanom file needs to have the same number of gray levels as materials defined in the
    #material_map. It also has to be a square iamge.
    '''
    def __init__(self, file_name, position_m, dx, material_map, energy_keV):
        self.file_name = file_name
        self.position_m = position_m
        self.material_map = material_map
        self.img = np.array(Image.open(file_name))
        self.Nx = self.img.shape[1]
        self.dx = dx
        self.size_m = self.Nx*dx
        self.x = np.linspace(-self.Nx/2, self.Nx/2, self.Nx)*dx
        self.mu = self.img*0
        self.electron_density = self.img*0
        self.phi = self.img*0
        self.diff = self.img*0
        self.density = self.img*0
        wave_m = h*c/(energy_keV*1e3*q_e) # Wavelength [m]
        self.transmission = None # These get calculated in the project method
        self.diffusion = None # These get calculated in the project method
        self.phase = None
        self.max_phase_difference = None
        self.max_phase_gradient = None

        
        for material in material_map.items():
            formula, density = xdb.get_material(material[1][0])
            delta, beta, att_cm = xdb.xray_delta_beta(formula, density, energy_keV*1e3)
            
            # att_cm from the function above does not reproduce values from NIST
            # material_mu with kind='total' does match NIST
            att_cm = 1/xdb.material_mu(material[1][0], energy_keV*1e3, kind='total')
            beta_total = wave_m/(4*np.pi*att_cm*1e-2)
            
            phi_cm = (2*np.pi*delta/wave_m)/1e2
            mu_cm = (4*np.pi*beta_total/wave_m)/1e2
            if len(material) == 3: delta = material[1][2]*(1e9)**3*r_e*wave_m**2/(2*np.pi) # If electron density given use it to calculate delta
            diff_cm = material[1][1]
            self.phi = np.where(self.img==material[0], phi_cm, self.phi)
            self.mu = np.where(self.img==material[0], mu_cm, self.mu)
            self.diff = np.where(self.img==material[0], diff_cm, self.diff)
            self.density = np.where(self.img==material[0], density, self.density)
            self.electron_density = np.where(self.img==material[0], (delta*2*np.pi/(r_e*wave_m**2))/((1e9)**3), self.electron_density)
            
        self.phi = gaussian(self.phi, sigma=1)
        self.mu = gaussian(self.mu, sigma=1)
        self.diff = gaussian(self.diff, sigma=1) 
        self.density = gaussian(self.density, sigma=1)
        
              
    def Project(self, angle_deg, x, xBin, setup):
        Nxi = x.samples
        dxi = x.det_dx/setup.object_magnification
        cpos = self.Nx/2 + 0.5/(self.dx/dxi/xBin) # Must use this rotation center to work with iradon of binned projections later on
        # print(cpos)
        img_mu = np.array(Image.fromarray(self.mu).rotate(angle_deg, resample=Image.Resampling.BILINEAR, fillcolor=self.mu[0,0], center=(cpos, cpos)))
        img_phi = np.array(Image.fromarray(self.phi).rotate(angle_deg, resample=Image.Resampling.BILINEAR, fillcolor=self.phi[0,0], center=(cpos, cpos)))
        img_diff = np.array(Image.fromarray(self.diff).rotate(angle_deg, resample=Image.Resampling.BILINEAR, fillcolor=self.diff[0,0], center=(cpos, cpos)))        
        
        xi = np.linspace(-Nxi/2, Nxi/2, Nxi)*dxi # Simulation coordiante at object plane (same as x.obj, not needed?)
        
        phi = np.sum(img_phi, axis=0)*self.dx*1e2 
        att = np.sum(img_mu, axis=0)*self.dx*1e2
        diff = np.sum(img_diff, axis=0)*self.dx*1e2
        
        # interpolate projections to simulation coordinates
        phii = interpolate.interp1d(self.x, phi, kind='linear', fill_value="extrapolate")
        atti = interpolate.interp1d(self.x, att, kind='linear', fill_value="extrapolate")
        diffi = interpolate.interp1d(self.x, diff, kind='linear', fill_value="extrapolate")
        
        phii = phii(xi) 
        atti = atti(xi)
        diffi = diffi(xi)
        
        # Smooth projections
        sigma = 0.5*self.dx # 0.5 normally
        # Kernel = np.exp(-0.5*(xi/sigma)**2)
        Kernel = np.where((xi>=-sigma)*(xi<sigma), 1, 0)
        
        phiib = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fft(Kernel)*np.fft.fft(phii))))
        attib = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fft(Kernel)*np.fft.fft(atti))))
        diffib = np.abs(np.fft.fftshift(np.fft.ifft(np.fft.fft(Kernel)*np.fft.fft(diffi))))
        
        phiib_sum = np.sum(phiib)
        attib_sum = np.sum(attib)
        diffib_sum = np.sum(diffib)
        
  
        if np.abs(phiib_sum) > 0: phiib = phiib*np.sum(phii)/phiib_sum
        if np.abs(attib_sum) > 0: attib = attib*np.sum(atti)/attib_sum
        if np.abs(diffib_sum) > 0: diffib = diffib*np.sum(diffi)/diffib_sum
  
        dphii = np.diff(phii, append=phii[-1])
        dphiib = np.diff(phiib, append=phiib[-1])
        
        # Plot projections for troubleshooting
        # plt.figure()
        # plt.plot(self.x/1e-6, phi, 'b.-', label='phi')
        # plt.plot(xi/1e-6, phii, 'r.-', label='phii')
        # plt.plot(xi/1e-6, phiib, 'g.-', label='phiib')
        # plt.title('Phantom phase projections')
        # plt.legend()

        self.phase = phiib
        self.max_phase_difference = np.abs(np.max(dphiib))
        self.max_phase_gradient = self.max_phase_difference/dxi
        self.transmission = np.sqrt(np.exp(-attib))*np.exp(-1j*phiib)
        self.diffusion = diffib # Variance of diffusion [rad^2]
        
        if self.max_phase_difference > 0.2:
            print('WARNING! Phase difference per Wavefront element is %.2f rad' % (np.max(np.abs(dphii))))
        
    def Show(self, auto=False):
        fig = plt.figure(figsize=(15,5))
        axs = fig.subplots(1, 3)
        if auto is False: 
            pcm = axs[0].pcolor(self.x/1e-2, self.x/1e-2, self.mu, cmap=cm.jet, shading='auto', vmin=0.205, vmax=0.235)
        else:
            pcm = axs[0].pcolor(self.x/1e-2, self.x/1e-2, self.mu, cmap=cm.jet, shading='auto')
        axs[0].set_aspect(1)
        axs[0].set_title('Attenuation coefficient [1/cm]')
        axs[0].set_xlabel('Distance [cm]')
        fig.colorbar(pcm, ax=axs[0], aspect=15, pad=0.15)
        if auto is False:
            pcm = axs[1].pcolor(self.x/1e-2, self.x/1e-2, self.phi, cmap=cm.jet, shading='auto', vmin=190, vmax=220)
        else:
            pcm = axs[1].pcolor(self.x/1e-2, self.x/1e-2, self.phi, cmap=cm.jet, shading='auto')
        axs[1].set_aspect(1)
        axs[1].set_title('Phase shift coefficient [1/cm]')
        axs[1].set_xlabel('Distance [cm]')
        fig.colorbar(pcm, ax=axs[1], aspect=15, pad=0.15)
        if auto is False:
            pcm = axs[2].pcolor(self.x/1e-2, self.x/1e-2, self.diff, cmap=cm.jet, shading='auto', vmin=0, vmax=0.6e-11)
        else:
            pcm = axs[2].pcolor(self.x/1e-2, self.x/1e-2, self.diff, cmap=cm.jet, shading='auto')
        axs[2].set_aspect(1)
        axs[2].set_title('Diffusion coefficient [1/cm]')
        axs[2].set_xlabel('Distance [cm]')
        fig.colorbar(pcm, ax=axs[2], aspect=15, pad=0.15)
    

@jit(forceobj=True, fastmath=True, parallel=True)
def Fit_curves(Curves):
    Shifts, Nc = Curves.shape
    
    Abs = np.zeros(Nc)
    Vis = np.zeros(Nc)
    Phi = np.zeros(Nc)

    for i in prange(Nc):
        c = Curves[:,i]
        C = np.fft.fft(c)/Shifts
        p = np.array([np.abs(C[0]), 2*np.abs(C[1])/np.abs(C[0]), np.angle(C[1])])
            
        Abs[i] = p[0]
        Vis[i] = p[1]
        Phi[i] = p[2]

    return Abs, Vis, Phi

def Curve_fit_func(x, m, v, phi):
    return m*(v*np.cos(2*np.pi*x + phi) + 1)

def Reconstruct_signals(scan_data, fitting_method):
    
    if fitting_method == 'DFT':
        Abs, Vis, Phi = Fit_curves(scan_data)

    else:
        Nc = scan_data.shape[1]
        
        Abs = np.zeros(Nc)
        Vis = np.zeros(Nc)
        Phi = np.zeros(Nc)

        p0 = [1, 0.2, 0] # Initial guess for fit
        limits = ([0, 0.001, -np.pi],[np.inf, 0.6, np.pi])
        xc = np.arange(scan_data.shape[0])/scan_data.shape[0]

        for i in prange(Nc):
            Ic = scan_data[:,i]
            
            p, pcov = curve_fit(Curve_fit_func, xc, Ic, p0, bounds=limits)
            
            p0 = p # Update guess for next pixel with current pixels solution 
            limits = ([0, 0.001, p0[2]-np.pi/2], [np.inf, 0.5, p0[2]+np.pi/2])
            
            Abs[i] = p[0]
            Vis[i] = p[1]
            Phi[i] = p[2]
    
    Phi = np.unwrap(Phi)
    return Abs, Vis, Phi


# def Reconstruct_CT_slice(sinogram, angles, detector):

