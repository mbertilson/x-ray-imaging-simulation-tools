1.2.4

- Removed need for passing detector object to Render_detector_image().
- Fixed issue with reconstructed signal noise due to inconsistent grating duty cycle in grating pattern.
- PIL Image rotation interplolation parameters updated to work with most recent PIL library.      

1.2.3

- Corrected error in transmission grating phase calulations.
- Source size is now defined as FWHM instead of standard deviation.
- Introduced a smooth parameter to model non-ideal gratings with smooth transition between bars and spaces.
- Detector object now has separate properties for approx_bin_size_um (use for defining detector) and bin_size_um (calculated).
- Simulation coordinate setup now has a minimum_padding parameter and zero padding is now >1x the propagation window.
- A number of detector PSFs are selectable via a shape parameter. Custom1 (1 um), Custom2 (2.5 um), Lorentzian and Gaussian.
- The pixel PSF can now be exluded in the system PSF via the include_pixel parameter (Set to False in Analyzer based imaging). 
- The PSF now also has a smooth parameter for modelling any additinal blurring in Analyzer based imaging.
- Added plots and print outs for trouble shooting and verification of simulation results. 
- Changed kernel in projection smoothing from Gaussian to top hat.
- The x-ray energy the imaging setup is designed for can now be different from the x-ray source.
- New phase recontruction correction which keeps the integrated phase gradient across the object at 0 while not introcucing a phase gradient ramp. 

1.2.2

- Issue with image intensity scaling before noise calculation for G2-based imaging resolved. 
- All signals (absorption, phase and darkfield) are now calculated relative to the illumination reference as in Bech 2010
- Quantitative reconstruction of linear diffusion coefficient as in Bech 2010
- Resolved issue with Diffusion filter not applied all the way to the edge of image. 
- Resolved issue with incorrect sampling of diffusion projeciton input to diffusion filter.
- Plot reconstructed vs actual diffusion projection in test mode.
- Simple object (rounded square) added with example code.

1.2.1

- Soft tissue ICRU-44 added to materials.
- Simulation code for comparison with Kirsten Taphorns results added, phantom included.
- Attenuation now pulled from material_mu() instead of xray_delta_beta(). Attenuation now agrees with NIST.
- The phantom object now has an electron density map. The electron density can be set in the material_map dictionary. If provided there, delta is calculated from the electron density.
- Phantom.Show() can take a auto=True/False parameter to select default or auto color scaling of displayed maps. 
- Diffusion filter convolution calculation corrected. 
- Get_air_energy_absorption() function added. Interpolates values from NIST in loglog space. Used for calculating air kerma.
- Air kerma at the object is calculated. 

1.2.0

- Simulates inverese geometry (G1 grating before the object) and does not assume G1 and object at same location.
- The interferometer setup is defined from G2 period and position instead of G1 period and source distance.
- Major code restructuring with python objects for all interferometer components and many functions moved to the library.
- Simulation broken up into two parts: Projections (Simulation) and Reconstruction. 
- Simulation results can be saved and loaded later so reconstruction conditions (e.g. dose) can be explored without 
the need to simulate the projecitons again.
- All the details about the simulated setup and the reconstruction conditions are saved in .txt files along with results.
- Test mode includes system info print out, propagation visualization etc
- Field grid size calculated to nearest size with prime factors 2 and/or 3 that fits the object (before it was just 2). 

1.1.2

- Trapeziodal numerical integration for increased phase reconstruciton accuracy
- 0.125 um sampling (before 0.25 um) removes gradient artifact in brain phase reconstructions
- Outputs the interferometer visibility with and without sample
- Test mode outputs RMS phase error
- Optional comb mask for phase stepping curve generation. Preserves visibility better than cosine mask.
- Rescaling of saved output tifs removed. 

1.1.1

- X-ray dose calculations for projections and CT reconstructions
- Sampling center adjusted, PSFs adjusted to remove shift after convolution
- More diagnostics and debug messages
- Correction to scaling of reconstructions so that it works with any magnificaiton (was fixed at M=2). 
- Optional save results to files

1.1.0

- Forcing number of Macro pixels will be even, needed for correct rotation center
- Clipping edges of projections to remove artifacts from edge effects in propagation
- Numba accelerated DFT based fitting function added for phase stepping curve fits, much faster than scipy curve_fit
- Wavefront sampling warning message displayed when sampling is insufficeint
- Moved reference image calculations outside and before the projection loop (speed up)
- Removed uncessesary phase step calculations (speed up)
- Linear diffusion coefficient property added to phantom model
- Diffusion projection added
- Numba accelerated diffusion filter for diffusion propagation added
- Diffusion modeling made optional
- Simulation parameter test mode added
- Simulations with 2^20 samples/projection runs 35x faster than v1.0.0

1.0.0
- Initial commit
