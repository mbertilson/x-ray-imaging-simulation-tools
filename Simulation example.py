

from DPC_Simulation import *
import numpy as np
import matplotlib.pylab as plt
import matplotlib.cm as cm
from scipy.integrate import cumtrapz
from skimage.transform import iradon
import time
from numba import jit, prange
from PIL import Image
import dill
from pathlib import Path


Simulation_data_folder = Path().cwd() / Path('Output data')

# Simulation parameters
Sim = Define_simulation(
    load_data = False, # Load already simulated projeciton data
    data_folder = Simulation_data_folder, 
    id_number = 'S02', 
    save_results = True, # Make sure you restart the kernel before running a simulation you want to load later (clears all objects).
    include_tifs = True,
    test_mode = False, # Will not save data and displays extra messages and plots, runs only one projection.
    ignore_diffusion = True,
    samples_per_pixel = 4
    )

Reconstruction_data_folder = Path().cwd() / Path('Output data')

# Reconstruction parameters

Rec = Define_reconstruction(
    add_noise = True, 
    total_dose_Gy = None, # If set to None the photons_per_pixel parameter must be set. Total dose is then calculated in simulation.
    photons_per_pixel = 1e5, 
    slice_thickness_m = 5e-3, # Settings related to noise calculations
    data_folder = Reconstruction_data_folder, 
    id_number = 'R02', 
    save_results = True, 
    mask_type = 'Square', 
    comb_ratio = 0.1, # Set Comb_ratio to 1/(number of scan steps) this only affects G2-less reconstruction. G2-based always uses 0.5)
    fitting_method = 'DFT', # 'DFT' or 'curve_fit'
    G2_based = False
    )


if Sim.load_data == False:
    # Imaging setup and geometry
    # Coordinate system with source and g0 at the origin. 
    # All positions of gratings, object and detector should be given relative to the source.
    # The G2_period must be an integer multiple of the detector pixel size for current fitting method to work.
    G2_scan_step_um = 32/10 # Set this to the sampling step size of the high-resolution detector

    Setup = Define_setup(energy_keV=60, talbot_order=1, G2_position_m=2.0, 
        G2_period_um=32, object_position_m=1.0)

    Source = Define_source(energy_keV=60, size_um=1000)

    G0 = Define_grating(position_m=0, material='Au', period_um=Setup.G0_period_um, 
        thickness_um=300, slit_period_ratio=0.25, energy_keV=Source.energy_keV)

    # G1 = Define_grating(position_m=Setup.G1_position_m, 
    #     material='Custom', period_um=Setup.G1_period_um, phase=np.pi, transmittance=1)

    G1 = Define_grating(position_m=Setup.G1_position_m, material='Si', period_um=Setup.G1_period_um, 
        thickness_um=77.19, slit_period_ratio=0.5, smooth_um=Setup.G1_period_um/10/4, energy_keV=Source.energy_keV)

    G2 = Define_grating(position_m=Setup.G2_position_m, material='Au', 
        period_um=Setup.G2_period_um, thickness_um=300, energy_keV=Source.energy_keV)

    # Define sample object by: gray level: ('Material name', linear diffucsion coefficienct [1/cm], electron density [1/nm3]). 
    # If Linear diffusion coefficients are zero diffusion calculations are skipped (runs faster).
    Material_map = {
        0:['air', 0], 
        1:['CSF', 0], 
        2:['grey matter', 0], 
        3:['white matter', 0], 
        4:['adipose 2', 0], 
        5:['muscle 2', 0], 
        6:['skin 2', 0], 
        7:['cortical bone', 1e-12], #cortical bone
        8:['blood whole', 0],
        9:['adipose 2', 0],
        10:['connective tissue', 0],
        11:['marrow red',0],
        12:['water', 0]}

    # Create phantom object
    Phantom = Define_phantom('Brain section 0.5 mm.tif', position_m=Setup.object_position_m, 
        dx=0.5e-3, material_map=Material_map, energy_keV=Source.energy_keV)

    # Define detector and coordinates
    Detector = Define_detector(pixel_size_um=G2_scan_step_um, PSF_width_um=2.5, 
        approx_bin_size_um=250, setup=Setup, phantom=Phantom)

    x = Define_coordinates(samples_per_pixel=Sim.samples_per_pixel, 
        setup=Setup, source=Source, detector=Detector)

    # Define projection angle
    Angles = Define_angles(N=36, range_deg=360, size_resolution_ratio=None) # 720

    # Calculate system PSFs
    if Rec.G2_based:
        PSF = Define_PSF(x=x, source=Source, G0=G0, detector=Detector, setup=Setup, shape='None', include_pixel=False, smooth=2)
    else:
        PSF = Define_PSF(x=x, source=Source, G0=G0, detector=Detector, setup=Setup, shape='Gaussian', include_pixel=True, smooth=0)


    # Calculate reference fields and kernels needed later (faster to calculate once than to recalculate for every projection angle)
    Field_Mask = np.ones(x.samples)
    Field_Mask[0:(x.padding - x.minimum_padding)] = 0 
    Field_Mask[-(x.padding - x.minimum_padding):] = 0
    Field_G1 = Define_EM_field(x_m=x.G1, wave_m=Source.wave_m, radius_m=G1.position_m, E=G1.Transmission(x.G1, 1))
    Field_G1.E = Field_G1.E*Field_Mask
    Field_G1.I = Field_G1.I*Field_Mask
    Field_obj = Field_G1.Propagate(distance_m=(Phantom.position_m - G1.position_m))
    kernel = Propagation_kernel(x_m=x.obj, wave_m=Source.wave_m, radius_m=Phantom.position_m, distance_m=(Detector.position_m-Phantom.position_m))
    Field_det = Field_obj.Propagate((Detector.position_m-Phantom.position_m), kernel)

    Illumination_det = Render_detector_image(x=x, I=Field_det.I, PSF=PSF.system)
    Setup.visibility = Get_visibility(Illumination_det)
    
    # Test mode specific data displays etc
    if Sim.test_mode: Angles = Define_angles(N=1)
    if Sim.test_mode: Phantom.Show()
    if Sim.test_mode: PSF.Show()
    if Sim.test_mode: Print_system_info(Sim, Setup, Source, G0, G1, Phantom, G2, Detector)
    # if Sim.test_mode: Propagation_test(x=x, setup=Setup, source=Source, G1=G1)

    print(' ')
    print('Interferometer visibility (no object, no G2 grating, @ detector sampling): %.3f' % (Setup.visibility))
    print('Max allowed projection phase gradient: %0.3f rad/um' % (Setup.max_acceptable_phase_gradient*1e-6))

    Image_data = np.zeros((Detector.pixels, Angles.N))
    Projection_data = np.zeros((Detector.pixels, Angles.N))

    t0 = time.time()
    for i in range(Angles.N):
        t1 = time.time()
        Phantom.Project(Angles.deg[i], x, Detector.pixels_per_bin*x.samples_per_pixel, Setup)
        if Sim.test_mode: Phantom.Project(15, x, Detector.pixels_per_bin*x.samples_per_pixel, Setup) # For testing specific angle
        Field_det = Define_EM_field(x_m=x.obj, wave_m=Source.wave_m, radius_m=Phantom.position_m, E=Field_obj.E*Phantom.transmission)
        Field_det = Field_det.Propagate((Detector.position_m-Phantom.position_m), kernel)
        Image_det = Render_detector_image(x=x, I=Field_det.I, PSF=PSF.system)

        if Sim.ignore_diffusion == False:
            Diffusion_sigma_pix = np.sqrt(Phantom.diffusion)*(Detector.position_m-Phantom.position_m)/(Detector.pixel_size_um*1e-6)
            Diffusion_sigma_pix = Render_detector_image(x=x, I=Diffusion_sigma_pix) # Down sampling to match image
            Image_det = Diffusion_filter(Image_det, Diffusion_sigma_pix)

        Image_data[:,i] = Image_det

        Projection_det = Render_detector_image(x=x, I=np.abs(Phantom.transmission)**2, PSF=PSF.system)
        Projection_data[:,i] = Projection_det
        
        t2 = time.time()
        print('Angle: %.2f deg' % (Angles.deg[i]), end = '')
        print(', projection time: %.2f s, total time: %0.2f s' % (t2-t1, t2-t0))

    # Clean up variables not needed, but keep them in test mode.
    if Sim.test_mode == False:
        del Image_det, Projection_det, kernel, Field_det, Field_G1, Field_obj

    if (Sim.save_results == True) and (Sim.test_mode == False) and (Sim.load_data == False):
        ''' 
        Objects needed for reconstruction later:
        Sim
        Rec
        Source
        Phantom
        G2
        x
        Detector
        Angles
        Illumination_det
        Image_data
        Projection_data
        
        
        
        '''
        
        dill.dump_session(Sim.data_folder / Path(Sim.id_number + ' - Simulation data.pkl'))
        
        
        
        
        
        Save_simulation_info(Sim.data_folder / Path(Sim.id_number + ' - Simulation info.txt'), Sim, Setup, Source, G0, G1, Phantom, G2, Detector)
        if Sim.include_tifs:
            Image.fromarray(Image_data).save(Sim.data_folder / Path(Sim.id_number + ' - Image data.tif'))
            Image.fromarray(Projection_data).save(Sim.data_folder / Path(Sim.id_number + ' - Projection data.tif'))
            Image.fromarray(Illumination_det).save(Sim.data_folder / Path(Sim.id_number + ' - Illuimation reference.tif'))
        print('Data saved to: ' + str(Sim.data_folder))


# Calculate dose, Air kerma, and add noise
if Rec.add_noise:
    Rec.slice_mass_kg = np.sum(Phantom.density*1e3)*Phantom.dx**2*Rec.slice_thickness_m

    if (Rec.total_dose_Gy is None):
        Rec.total_dose_Gy = Source.photon_energy_J*Rec.photons_per_pixel*np.sum(1 - Image_data)/Rec.slice_mass_kg
    
    Rec.photons_per_pixel = Rec.total_dose_Gy*Rec.slice_mass_kg/(Source.photon_energy_J*np.sum(1 - Image_data))    
    Rec.projection_dose_Gy = Rec.total_dose_Gy/Angles.N
    Rec.photons_per_bin = Rec.photons_per_pixel*Detector.pixels_per_bin


    # Calculating air kerma
    mu_en_air = Get_air_energy_absorption(Source.energy_keV) # cm2/g
    Rec.bin_area_cm2 = Detector.pixels_per_bin * Detector.pixel_size_um*(1e2/1e6) * Rec.slice_thickness_m*1e2 * Setup.object_magnification
    Rec.detector_photons_per_cm2 = Rec.photons_per_bin / Rec.bin_area_cm2
    Rec.object_photons_per_cm2 = Rec.detector_photons_per_cm2 * Setup.object_magnification**2
    Rec.object_air_kerma = Rec.object_photons_per_cm2 * mu_en_air*1000 * Source.photon_energy_J


# Diagnostics
if True:    
    print('Phantom phase difference per wavefront element (max): %.3f rad' % (Phantom.max_phase_difference))
    print('Phantom phase gradient (max): %.3f rad/um' % (Phantom.max_phase_gradient*1e-6))

    if Rec.add_noise:
        print('Minimum transmission: %.6f ' % (np.min(np.abs(Phantom.transmission)**2)))
        print('Projection exposure: %0.0f photons/pixel, %0.0f photons/bin' % (Rec.photons_per_pixel, Rec.photons_per_bin))
        print('Projection dose: %.4f mGy' % (Rec.projection_dose_Gy*1e3))
        print('Projection air kerma: %.4f mGy' % (Rec.object_air_kerma*1e3))
        print('Total dose: %0.2f mGy' % (Rec.total_dose_Gy*1e3))

    plt.figure('Image at detector')
    plt.plot(x.pix/1e-3, Illumination_det,'y', label='Illumination')
    plt.plot(x.pix/1e-3, Image_data[:,-1],'b', label='Image')
    plt.ylim((0, 1.05*np.max(Illumination_det)))
    plt.ylabel('Image intensity')
    plt.xlabel('Distance [mm]')
    plt.legend()

Shifts = Detector.pixels_per_G2_period
Mask_scan_data = np.zeros((Shifts, Detector.bins))
Mask_scan_reference = np.zeros((Shifts, Detector.bins))

# Defining signal data containers 
Abs_bin = np.zeros((Detector.bins, Angles.N))
Vis_bin = np.zeros((Detector.bins, Angles.N))
Phi_bin = np.zeros((Detector.bins, Angles.N))
Theta_bin = np.zeros((Detector.bins, Angles.N))
Projection_bin = np.zeros((Detector.bins, Angles.N))
Diff_sigma_bin = np.zeros((Detector.bins, Angles.N))

# Mask used in G2-less reconstruction
Mask = Define_grating(
    position_m=Detector.position_m, material='Custom', period_um=G2.period_um,
    shape=Rec.mask_type, slit_period_ratio=Rec.comb_ratio, phase=0, transmittance=0)

# Generate phase stepping curves with or without G2
t0 = time.time()

for i in range(Angles.N):
    t1 = time.time()

    if Rec.G2_based:
        # Calculates curves obtained by scanning a G2 grating. Each image in the scan has uniqe noise.
        if Sim.test_mode: print('Generating G2 grating scanning data')

        for Shift in range(Shifts):
            Mask_shifted = np.abs(G2.Transmission(x.pix, 1, G2.period_um*1e-6*(Shift/Shifts)))**2
            Image_pix = Image_data[:,i]*Mask_shifted
            
            if Rec.add_noise: 
                Image_pix = Add_noise(Image_pix*Rec.photons_per_pixel/Shifts)/(Rec.photons_per_pixel/Shifts)/Mask_shifted.mean() # Divide the number of photons per pixel with number of shifts
            else:
                Image_pix = Image_pix/Mask_shifted.mean()
                
            Image_bin = Image_pix.reshape(-1, Detector.pixels_per_bin).mean(axis=1)
            Mask_scan_data[Shift,:] = Image_bin
            
            if i == 0:
                Reference_pix = Illumination_det*Mask_shifted/Mask_shifted.mean() # Reference
                Reference_bin = Reference_pix.reshape(-1, Detector.pixels_per_bin).mean(axis=1)
                Mask_scan_reference[Shift,:] = Reference_bin

    else:
        # Calculates curves for the case where one image is acquired and the G2 is applied numerically.
        if Sim.test_mode: print('Generating mask scanning data. Mask type: ' + Rec.mask_type)

        Image_pix = Image_data[:,i]
        if Rec.add_noise:
            Image_pix = Add_noise(Image_pix*Rec.photons_per_pixel)/Rec.photons_per_pixel

        for Shift in range(Shifts):
            Mask_shifted = np.abs(Mask.Transmission(x.pix, 1, Mask.period_um*1e-6*(Shift/Shifts)))**2
            Masked_Image_pix = Image_pix*Mask_shifted/Mask_shifted.mean()
            Image_bin = Masked_Image_pix.reshape(-1, Detector.pixels_per_bin).mean(axis=1)
            Mask_scan_data[Shift,:] = Image_bin
            
            if i == 0:
                Reference_pix = Illumination_det*Mask_shifted/Mask_shifted.mean() 
                Reference_bin = Reference_pix.reshape(-1, Detector.pixels_per_bin).mean(axis=1)
                Mask_scan_reference[Shift,:] = Reference_bin

    if Sim.test_mode:
        Norm_scan_data = Normalize_scan_data(Mask_scan_data)
        
        plt.figure('Image at detector')
        if Rec.G2_based: 
            plt.plot(x.pix/1e-3, Image_pix*Mask_shifted.mean(),'g', label='Masked noisy image')
        else:
            plt.plot(x.pix/1e-3, Image_pix*Mask_shifted,'g', label='Masked noisy image')
        plt.legend()
        
        plt.figure()
        # plt.pcolor(x.bin/1e-3, np.arange(Shifts*1), Norm_scan_data, cmap=cm.gray, shading='auto')
        plt.pcolor(x.bin/1e-3, np.arange(Shifts*1), Mask_scan_data, cmap=cm.gray, shading='auto')
        plt.colorbar(aspect=15, pad=0.15)
        plt.title('Normalized mask/G2 stepping curves')
        plt.xlabel('x [mm]')
        plt.ylabel('Scan step number')

        plt.figure()
        center_bin = int(Detector.bins/2)
        plt.plot(Mask_scan_reference[:, (center_bin-2):(center_bin+1)],'k', label='Reference')
        plt.plot(Mask_scan_data[:, (center_bin-2):(center_bin+1)], label='With object')
        plt.ylim([0, 1.1*np.max(Mask_scan_reference[:, (center_bin-2):(center_bin+1)])])
        plt.legend()
        plt.xlabel('Scan step number')
        plt.title('Analyzer/Mask scan curves')

    # Extract image signals from scan data, passing the container varables should be faster (?)
    if i == 0: Abs_reference, Vis_reference, Phi_reference = Reconstruct_signals(scan_data=Mask_scan_reference, fitting_method=Rec.fitting_method)
    Abs, Vis, Phi = Reconstruct_signals(scan_data=Mask_scan_data, fitting_method=Rec.fitting_method)
    
    Projection_bin[:,i] = Projection_data[:,i].reshape(-1, Detector.pixels_per_bin).mean(axis=1)
    Abs_bin[:,i] = Abs/Abs_reference
    Vis_bin[:,i] = Vis/Vis_reference
    Phi_bin[:,i] = Phi_reference - Phi

    # Corrections to Phi to force the phase shift at edges to 0
    # Phi_bin[:,i] = Phi_bin[:,i] - np.mean(Phi_bin[:,i]) # If to constain the Phase shift at edges to 0.
    
    # Alternative corrections to Phi to force the phase shift at edges to 0
    pos_sum = Phi_bin[Phi_bin[:,i]>0, i].sum()
    neg_sum = -Phi_bin[Phi_bin[:,i]<0, i].sum()
    avg_sum = (pos_sum + neg_sum)/2
    Phi_bin[Phi_bin[:,i]>0, i] = Phi_bin[Phi_bin[:,i]>0, i]*avg_sum/pos_sum
    Phi_bin[Phi_bin[:,i]<0, i] = Phi_bin[Phi_bin[:,i]<0, i]*avg_sum/neg_sum

    Theta_bin[:,i] = cumtrapz(Phi_bin[:,i], dx=1, axis=0, initial=0) \
        *Detector.obj_bin_size_um*1e-6*(G2.period_um*1e-6) \
        /(Source.wave_m*(Detector.position_m-Phantom.position_m))
    Diff_sigma_bin[:,i] = np.sqrt(-np.log(Vis_bin[:,i])*(G2.period_um*1e-6)**2/(2*np.pi**2*(Detector.position_m-Phantom.position_m)**2))
    Diff_sigma_bin[np.isnan(Diff_sigma_bin)] = 0

    t2 = time.time()
    print('Angle: %.2f deg' % (Angles.deg[i]), end = '')
    print(', signal reconstruction time: %.2f s, total time: %0.2f s' % (t2-t1, t2-t0))



if Sim.test_mode:
    Theta_reference = np.interp(x.bin, x.det, Phantom.phase)
    RMS_phase_error = np.sqrt(np.mean((Theta_bin[:,-1] - Theta_reference)**2))
    Phi_ref = np.diff(Phantom.phase, append=Phantom.phase[-1])/(x.det_dx*1e6)
    Phi_rec = np.diff(Theta_bin[:,-1], append=Theta_bin[-1,-1])/Detector.bin_size_um

    plt.figure()
    plt.plot(x.bin/1e-3/Setup.object_magnification, Phi_rec, 'b', linewidth=1, label='Reconstruction')
    plt.plot(x.det/1e-3/Setup.object_magnification, Phi_ref, 'r', linewidth=1, label='Actual' )
    plt.ylabel('Phase gradient [rad/um]')
    plt.xlabel('x [mm]')
    plt.title('Phase gradient projection')
    plt.legend()

    plt.figure()
    plt.plot(x.bin/1e-3/Setup.object_magnification, Theta_bin[:,-1], 'b', linewidth=1, label='Reconstruction')
    plt.plot(x.det/1e-3/Setup.object_magnification, Phantom.phase, 'r', linewidth=1, label='Actual' )
    plt.plot(x.bin/1e-3/Setup.object_magnification, Theta_bin[:,-1] - Theta_reference,'g',linewidth=1, label='Error'  )
    plt.ylabel('Phase [rad]')
    plt.xlabel('x [mm]')
    plt.title('Phase projection')
    plt.legend()

    plt.figure()
    plt.plot(x.bin/1e-3/Setup.object_magnification, Abs_bin[:,-1], 'b', linewidth=1, label='Reconstruction')
    plt.plot(x.det/1e-3/Setup.object_magnification, np.abs(Phantom.transmission)**2, 'r', linewidth=1, label='Actual' )
    plt.ylabel('Intensity')
    plt.xlabel('x [mm]')
    plt.title('Absorption projection')
    plt.legend()

    plt.figure()
    plt.plot(x.bin/1e-3/Setup.object_magnification, Diff_sigma_bin[:,-1], 'b', linewidth=1, label='Reconstruction')
    plt.plot(x.det/1e-3/Setup.object_magnification, np.sqrt(Phantom.diffusion), 'r', linewidth=1, label='Actual' )
    plt.ylabel('Diffusion standard deviation [rad]')
    plt.xlabel('x [mm]')
    plt.title('Diffusion coefficient projection')
    plt.legend()
    
    print('Reference visibility (from fitting): %0.3f' % (Vis_reference.mean()))
    print('Minimum visibility (from fitting) relative to reference: %.3f' % (Vis_bin.min()))
    print('RMS phase error: %.2f rad' % (RMS_phase_error))


Log_Abs_bin = -np.log(Abs_bin)
Log_Abs_bin[np.isnan(Log_Abs_bin)] = 0 # Makes sure any nan's are set to 0 before reconstruction

Log_Projection_bin = -np.log(Projection_bin)
Log_Projection_bin[np.isnan(Log_Projection_bin)] = 0

if Sim.test_mode == False:    
    t0 = time.time()
    Abs_rec = iradon(Log_Abs_bin, theta=Angles.deg, interpolation='linear', filter_name='cosine')*1e-2/(Detector.obj_bin_size_um*1e-6)
    Diff_rec = iradon(Diff_sigma_bin**2, theta=Angles.deg, interpolation='linear', filter_name='cosine')*1e-2/(Detector.obj_bin_size_um*1e-6)
    Phi_rec = iradon(Theta_bin, theta=Angles.deg, interpolation='linear', filter_name='cosine')*1e-2/(Detector.obj_bin_size_um*1e-6)
    Projection_rec = iradon(Log_Projection_bin, theta=Angles.deg, interpolation='linear', filter_name='cosine')*1e-2/(Detector.obj_bin_size_um*1e-6)
    t1 = time.time()
    print('Reconstruction time: %.2f s' % (t1-t0))
    print('Mean visibility relative to reference: %.3f s' % (Vis_bin.mean()))
    
    
if Sim.test_mode == False:
    center_bin = int(Detector.bins/2)    
    fig = plt.figure(figsize=(20,5))
    axs = fig.subplots(1, 5)
    Norm_scan_data = Normalize_scan_data(Mask_scan_data)
    axs[0].pcolor(Norm_scan_data, cmap=cm.gray, shading='auto')
    axs[0].set_aspect(Mask_scan_data.shape[1]/Mask_scan_data.shape[0])
    axs[0].set_title('Mask/G2 grating scan data')
    axs[1].plot(Mask_scan_data[:, (center_bin-2):(center_bin+2)])
    axs[1].set_ylim([0, np.max(Mask_scan_data[:, (center_bin-2):(center_bin+2)])])
    axs[1].set_aspect(Shifts/np.max(Mask_scan_data[:, (center_bin-2):(center_bin+2)]))
    axs[1].set_title('Mask/G2 grating scan curves')
    axs[2].pcolor(Phi_bin, cmap=cm.gray, shading='auto')
    axs[2].set_aspect(Phi_bin.shape[1]/Phi_bin.shape[0])
    axs[2].set_title('Phi')
    axs[3].pcolor(Theta_bin, cmap=cm.gray, shading='auto')
    axs[3].set_aspect(Theta_bin.shape[1]/Theta_bin.shape[0])
    axs[3].set_title('Phase sinogram')
    axs[4].pcolor(Phi_rec, cmap=cm.gray, shading='auto')
    axs[4].set_aspect(1)
    axs[4].set_title('Phase signal reconstruction')
    
    
    fig = plt.figure(figsize=(10,5))
    axs = fig.subplots(1, 2)
    axs[0].pcolor(Phi_rec, cmap=cm.gray, shading='auto', vmin=195, vmax=210)
    axs[0].set_aspect(1)
    axs[0].set_title('Phase reconstruction [1/cm]')
    axs[1].pcolor(Theta_bin, cmap=cm.gray, shading='auto')
    axs[1].set_aspect(Theta_bin.shape[1]/Theta_bin.shape[0])
    axs[1].set_title('Phase sinogram')
    
    
    fig = plt.figure(figsize=(10,5))
    axs = fig.subplots(1, 2)
    axs[0].pcolor(Abs_rec, cmap=cm.gray, shading='auto', vmin=0.205, vmax=0.235)
    axs[0].set_aspect(1)
    axs[0].set_title('Attenuation reconstruction [1/cm]')
    axs[1].pcolor(Log_Abs_bin, cmap=cm.gray, shading='auto')
    axs[1].set_aspect(Log_Abs_bin.shape[1]/Log_Abs_bin.shape[0])
    axs[1].set_title('Absorption sinogram')
    
    
    fig = plt.figure(figsize=(10,5))
    axs = fig.subplots(1, 2)
    axs[0].pcolor(Diff_rec, cmap=cm.gray, shading='auto')
    axs[0].set_aspect(1)
    axs[0].set_title('Diffusion coefficient reconstruction [1/cm]')
    axs[1].pcolor(Diff_sigma_bin, cmap=cm.gray, shading='auto')
    axs[1].set_aspect(Diff_sigma_bin.shape[1]/Diff_sigma_bin.shape[0])
    axs[1].set_title('Diffusion sinogram')
    
    
    fig = plt.figure(figsize=(10,5))
    axs = fig.subplots(1, 2)
    axs[0].pcolor(Projection_rec, cmap=cm.gray, shading='auto', vmin=0.195, vmax=0.235)
    axs[0].set_aspect(1)
    axs[0].set_title('Parallel projection reconstruction (no noise)')
    axs[1].pcolor(Log_Projection_bin, cmap=cm.gray, shading='auto')
    axs[1].set_aspect(Log_Projection_bin.shape[1]/Log_Projection_bin.shape[0])
    axs[1].set_title('Parallel projection sinogram (no noise)')
    

    if (Rec.save_results == True) and (Sim.test_mode == False):
        with open(Rec.data_folder / Path(Sim.id_number + Rec.id_number + ' - Reconstruction info.txt'),'w') as file:
            print('\nReconstruction parameters:\n', file=file)
            pprint(vars(Rec), file)

        Image.fromarray(Abs_rec).save(Rec.data_folder / Path(Sim.id_number + Rec.id_number + ' - Reconstruction - Absorption.tif'))  
        Image.fromarray(Phi_rec).save(Rec.data_folder / Path(Sim.id_number + Rec.id_number + ' - Reconstruction - Phase.tif'))  
        Image.fromarray(Diff_rec).save(Rec.data_folder / Path(Sim.id_number + Rec.id_number + ' - Reconstruction - Dark field.tif'))
        Image.fromarray(Projection_rec).save(Rec.data_folder / Path(Sim.id_number + Rec.id_number + ' - Reconstruction - Projection.tif'))  

        print('Reconstruction results saved to: ' + str(Rec.data_folder))




