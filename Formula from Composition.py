import numpy as np
import xraydb as xdb
import requests
import re    
from bs4 import BeautifulSoup

'''
Compostion data can be found at:
1) https://physics.nist.gov/PhysRefData/XrayMassCoef/tab2.html
2) The composition of body tissues, The British Journal of Radiology (1986)

Use the search word for retrieving data from NIST. 
To manually enter weight ratios set search word to empty string.

The script calculates the Atomic composition of the material and outputs the material formula.
    
'''

White_matter = [[1, 10.6], [6, 19.4], [7, 2.5], [8, 66.1], [11, 0.2], [15, 0.4], [16, 0.2], [17, 0.3], [19, 0.3]] 
CSF = [[1, 11.1], [8, 88], [11, 0.5], [17, 0.4]] 

Adipose_1 = [[1, 11.2], [6, 51.7], [7, 1.3] ,[8, 35.5], [11, 0.1], [16, 0.1], [17, 0.1]]
Adipose_2 = [[1, 11.4], [6, 59.8], [7, 0.7] ,[8, 27.8], [11, 0.1], [16, 0.1], [17, 0.1]] 
Adipose_3 = [[1, 11.6], [6, 68.1], [7, 1.2] ,[8, 19.8], [11, 0.1], [16, 0.1], [17, 0.1]] 

Muscle_1 = [[1, 10.1], [6, 17.1], [7, 3.6] ,[8, 68.1], [11, 0.1], [15, 0.2], [16, 0.3], [17, 0.1], [19, 0.4]] 
Muscle_2 = [[1, 10.2], [6, 14.3], [7, 3.4] ,[8, 71], [11, 0.1], [15, 0.2], [16, 0.3], [17, 0.1], [19, 0.4]] 
Muscle_3 = [[1, 10.2], [6, 11.2], [7, 3.0] ,[8, 74.5], [11, 0.1], [15, 0.2], [16, 0.3], [17, 0.1], [19, 0.4]] 

Skin_1 = [[1, 10], [6, 25], [7, 4.6] ,[8, 59.4], [11, 0.2], [15, 0.1], [16, 0.3], [17, 0.3], [19, 0.1]] 
Skin_2 = [[1, 10], [6, 20.4], [7, 4.2] ,[8, 64.5], [11, 0.2], [15, 0.1], [16, 0.2], [17, 0.3], [19, 0.1]] 
Skin_3 = [[1, 10.1], [6, 15.8], [7, 3.7] ,[8, 69.5], [11, 0.2], [15, 0.1], [16, 0.2], [17, 0.3], [19, 0.1]] 

Cortical_bone = [[1, 3.4], [6, 15.5], [7, 4.2] ,[8, 43.5], [11, 0.1], [12, 0.2], [15, 10.3], [16, 0.3], [20, 22.5]] 

Spongiosa = [[1, 8.5], [6, 40.4], [7, 2.8] ,[8, 36.7], [11, 0.1], [12, 0.1], [15, 3.4], [16, 0.2], [17, 0.2], [19, 0.1],[20, 7.4],[26, 0.1]]
Marrow_red = [[1, 10.5], [6, 41.4], [7, 3.4] ,[8, 43.9], [15, 0.1], [16, 0.2], [17, 0.2], [19, 0.2],[26, 0.1]]
Marrow_yellow = [[1, 11.5], [6, 64.4], [7, 0.7] ,[8, 23.1], [11, 0.1], [16, 0.1], [17, 0.1]]

Blood_whole = [[1, 10.2], [6, 11], [7, 3.3] ,[8, 74.5], [11, 0.1], [15, 0.1], [16, 0.2], [17, 0.3],[19, 0.2],[26, 0.1]]
Connective_tissue = [[1, 9.4], [6, 20.7], [7, 6.2] ,[8, 62.2], [11, 0.6], [16, 0.6], [17, 0.3]]

weight_ratios = Adipose_2

density = 950 # [g/cm3]

search_word = 'Water'

    
if len(search_word) > 0:
    
    url = 'https://physics.nist.gov/PhysRefData/XrayMassCoef/tab2.html'    
    soup = BeautifulSoup(requests.get(url, headers={'User-Agent': 'Mozilla/5.0'}).text, 'html.parser')
    s = soup.find('td', string=re.compile(search_word))
    
    material = s.parent.find_all('td')[0].text
    density = float(s.parent.find_all('td')[3].text)
    composition = re.split('\r\n|:', s.parent.find_all('td')[4].text)
    composition = np.array(composition, dtype=float).reshape((-1, 2))
else:
    composition = np.array(weight_ratios, dtype=float)
    material = ''
    composition[:,1] = composition[:,1]/np.sum(composition[:,1])

n = np.zeros(len(composition))
formula = ''


print('Material: ' + material)
print('Density: %1.4f [g/cm3]' % (density))

print('Element | Atomic ratio | Weight ratio')
for i in range(0, len(composition)):
    Z = int(composition[i][0])
    n[i] = composition[i][1]/xdb.atomic_mass(Z)
    # print('%2.0f  %2s       %1.5f        %1.5f' % (Z, xdb.atomic_symbol(Z), n[i], composition[i][1]))
    # formula = formula + ' ' + xdb.atomic_symbol(Z) + '{:.5f}'.format(n[i]) 

n = n/np.sum(n)

for i in range(0, len(composition)):
    Z = int(composition[i][0])
    print('%2.0f  %2s       %1.5f        %1.5f' % (Z, xdb.atomic_symbol(Z), n[i], composition[i][1]))
    formula = formula + ' ' + xdb.atomic_symbol(Z) + '{:.5f}'.format(n[i]) 


print('Formula: ' + formula)

    

             
